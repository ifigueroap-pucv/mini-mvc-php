<?php

error_reporting(-1);
ini_set('display_errors', 'On');

class Colegio {
    
    public $idColegio;
    public $nombreColegio;
    public $nombreDirector;
    public $nombreSostenedor;
    // public $mensualidad;
    // public $dependencia;
    
    public function __construct($idColegio, $nombreColegio, $nombreDirector, $nombreSostenedor) {
        $this->nombreColegio = $nombreColegio;
        $this->nombreDirector = $nombreDirector;
        $this->nombreSostenedor = $nombreSostenedor;
        $this->idColegio = $idColegio;
    }        
}

class ColegioORM {
    
    public static function getById($idColegio) {        
        $db = new SQLite3("colegios.sqlite");
        $results = $db->query("SELECT * FROM Colegio WHERE id = {$idColegio}");
        
        if ($datos = $results->fetchArray(SQLITE3_ASSOC)) {
            return new Colegio($datos['id'], $datos['nombreColegio'], $datos['nombreDirector'], $datos['nombreSostenedor']);            
        } else {
            return null;
        }
    }
}

?>