<?php

error_reporting(-1);
ini_set('display_errors', 'On');

include('modelo.php');
include('vistas.php');

class ColegiosController {
    
    function handleAction($action) {
        $matches = array();        
       
        if (preg_match('#/colegios/(?P<idColegio>\d+)#', $action, $matches)) {
            $colegio = ColegioORM::getById($matches['idColegio']);
            if ($colegio != null) {
                echo (new VistaColegio())->renderizarInterfaz($colegio);
            } else {
                echo "ERROR: No se encuentra colegio con identificador {$colegioId}";
                http_response_code(404);
            }           
            
            
            
        } else {
            echo "ERROR: No se conoce como manejar la acción {$action}";
            http_response_code(404);   
        }       
    }
}

?>
